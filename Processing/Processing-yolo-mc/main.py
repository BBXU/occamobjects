import cv2
import numpy as np
import os
import json
import time

def get_output_layers(net):
    layer_names = net.getLayerNames()

    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers


def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h,classes,COLORS):
    label = str(classes[class_id])

    color = COLORS[class_id]

    cv2.rectangle(img, (x, y), (x_plus_w, y_plus_h), color, 2)

    cv2.putText(img, label, (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

def process(image,classes, COLORS, net,targets):
    Width = image.shape[1]
    Height = image.shape[0]
    scale = 0.00392

    blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)
    net.setInput(blob)

    outs = net.forward(get_output_layers(net))

    class_ids = []
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4
    target_index=[]

    for target in targets:
        if target != "all":
            target_index.append(classes.index(target))


    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5 and ("all" in targets or class_id in target_index):
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])

    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        draw_prediction(image, class_ids[i], confidences[i], round(x), round(y), round(x + w), round(y + h),classes,COLORS)


begining=time.time()
print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']

        config_information = manifest['inputs'][1]['connections'][0]
        config_root_path = config_information['paths']['mount']
        config_name = config_information['file']
        config_path = os.path.join(config_root_path, config_name)


        option_path=os.path.join(manifest['inputs'][2]['connections'][0]['paths']['mount'], manifest['inputs'][2]['connections'][0]['file'])
    except (KeyError, IndexError) as e:
        input_path = None

with open(option_path) as f:
    option_data=json.load(f)

target_option=None
targets=[]

for k,v in option_data.items():
    if k=="Target":
        target_option=v


for k,v in target_option.items():
    if v:
        targets.append(k)

yoloConfig_name = "yolov3.cfg"
yoloClass_name = "yolov3.txt"
yoloWeight_name = "yolov3.weights"

yoloConfig = os.path.join(config_path, yoloConfig_name)
yoloClass = os.path.join(config_path, yoloClass_name)
yoloWeight = os.path.join(config_path, yoloWeight_name)

images_path = os.path.join(input_path, input_name)

# Function code starts here
if images_path == None:
    print("images_path not found")

# initialize result
processed_images = []

# load images
images = np.load(images_path, allow_pickle=True)
classes = None

print("images:")
print(images)

print("yoloClass:" + yoloClass)
with open(yoloClass) as f:
    classes = [line.strip() for line in f.readlines()]

COLORS = np.random.uniform(0, 255, size=(len(classes), 3))

net = cv2.dnn.readNet(yoloWeight, yoloConfig)
start_time=time.time()
for image in images:
    process(image, classes, COLORS, net,targets)
    processed_images.append(image)



end_time=time.time()

print("IO NOT INCLUDED running time:",end_time-start_time)
print("processed:")
print(processed_images)

#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="np_images.npy"
output_file=os.path.join(output_path,output_file)

#Save to file
np.save(output_file,processed_images)

ending=time.time()
print("IO INCLUDED running time:",ending-begining)



