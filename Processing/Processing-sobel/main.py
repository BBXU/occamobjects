import os
import cv2
import numpy as np
import json
import time



def sobelOperator(img):
    container = np.copy(img)
    size = container.shape
    for i in range(1, size[0] - 1):
        for j in range(1, size[1] - 1):
            gx = (img[i - 1][j - 1] + 2*img[i][j - 1] + img[i + 1][j - 1]) - (img[i - 1][j + 1] + 2*img[i][j + 1] + img[i + 1][j + 1])
            gy = (img[i - 1][j - 1] + 2*img[i - 1][j] + img[i - 1][j + 1]) - (img[i + 1][j - 1] + 2*img[i + 1][j] + img[i + 1][j + 1])
            container[i][j] = min(255, np.sqrt(gx**2 + gy**2))
    return container
    pass

begining=time.time()
print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:
        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']

    except (KeyError, IndexError) as e:
        input_path = None


images_path = os.path.join(input_path, input_name)


# Function code starts here
if images_path == None:
    print("images_path not found")

# initialize result
processed_images = []

# load images
images = np.load(images_path, allow_pickle=True)

start_time=time.time()
for image in images:
    img = sobelOperator(image)
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    processed_images.append(img)

end_time=time.time()

print("IO NOT INCLUDED running time:",end_time-start_time)

#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="np_images.npy"
output_file=os.path.join(output_path,output_file)

#Save to file
np.save(output_file,processed_images)
ending=time.time()
print("IO INCLUDED running time:",ending-begining)

