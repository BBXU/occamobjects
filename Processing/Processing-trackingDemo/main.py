import time
import cv2
import json
import os
import sys
import numpy as  np


print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        detector_information = manifest['inputs'][0]['connections'][0]
        detector_path = detector_information['paths']['mount']
        print('detector_path:',detector_path)
        detector_folder = detector_information['file']
        detector_search_path=os.path.join(detector_path,detector_folder)
        print("detector_search_path:",detector_search_path)

        input_information = manifest['inputs'][1]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        video_path=os.path.join(input_path,input_name)
        print("video_path:",video_path)

        tracker_information=manifest['inputs'][2]['connections'][0]
        print('tracker_info')
        tracker_path=tracker_information['paths']['mount']
        print('tracker_path',tracker_path)
        tracker_folder=tracker_information['file']
        print('tracker_folder')
        tracker_search_path=os.path.join(tracker_path,tracker_folder)
        print('tracker_search_path',tracker_search_path)
    except (KeyError, IndexError) as e:
        input_path = None


sys.path.append(detector_search_path)
sys.path.append(tracker_search_path)

from yoloDetector import YoloDetector
from centroidtracker import CentroidTracker

video=np.load(video_path,allow_pickle=True)
processed_images = []

ct=CentroidTracker()
detector=YoloDetector(detector_search_path)


for frame in video:
    rects=detector.detect(frame)
    for rect in rects:
        (startX, startY, endX, endY) = rect
        cv2.rectangle(frame, (startX, startY), (endX, endY),
                      (0, 255, 0), 2)

    objects=ct.update(rects)

    for (objectID, centroid) in objects.items():
        # draw both the ID of the object and the centroid of the
        # object on the output frame
        text = "ID {}".format(objectID)
        cv2.putText(frame, text, (centroid[0] - 10, centroid[1] - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.circle(frame, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

    processed_images.append(frame)


#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="np_images.npy"
output_file=os.path.join(output_path,output_file)

#Save to file
np.save(output_file,processed_images)



