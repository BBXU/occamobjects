import cv2
import numpy as np
import os

class YoloDetector():
    def __init__(self,path):
        weight_file=os.path.join(path,"yolov3.weights")
        config_file=os.path.join(path,"yolov3.cfg")
        classes_file=os.path.join(path,"yolov3.txt")
        #read net
        self.net=cv2.dnn.readNet(weight_file,config_file)

        #read classes
        with open(classes_file, 'r') as f:
            self.classes = [line.strip() for line in f.readlines()]

        # Colors
        self.COLORS=np.random.uniform(0, 255, size=(len(self.classes), 3))

    def get_output_layers(self):
        layer_names = self.net.getLayerNames()

        output_layers = [layer_names[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]

        return output_layers


    def detect(self,image):
        Width=image.shape[1]
        Height=image.shape[0]
        scale=0.00392

        blob = cv2.dnn.blobFromImage(image, scale, (416, 416), (0, 0, 0), True, crop=False)
        self.net.setInput(blob)

        outs=self.net.forward(self.get_output_layers())

        class_ids = []
        confidences = []
        boxes = []
        conf_threshold = 0.75
        nms_threshold = 0.4

        rects=[]

        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > conf_threshold and class_id==0:
                    center_x = int(detection[0] * Width)
                    center_y = int(detection[1] * Height)
                    w = int(detection[2] * Width)
                    h = int(detection[3] * Height)
                    startX = center_x - w / 2
                    startY = center_y - h / 2
                    endX=center_x + w / 2
                    endY=center_y + h / 2
                    box=np.array([startX,startY,endX,endY])
                    rects.append(box.astype("int"))

        return rects

