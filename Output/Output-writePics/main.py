import os
import numpy as np
import cv2
import json
import time


begining=time.time()
print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        
    except (KeyError, IndexError) as e:
        input_path = None

images_path=os.path.join(input_path,input_name)


# load images
images = np.load(images_path, allow_pickle=True)
print(images)

#output path
output_path=os.path.join("..","outputs", "0", "0")

count = 1
for image in images:
    file_name = str(count) + ".jpg"
    output_file = os.path.join(output_path, file_name)
    cv2.imwrite(output_file, image)
    print(file_name)
    count = count + 1

ending=time.time()

print("running time:",ending-begining)
