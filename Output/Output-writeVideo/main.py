import os
import numpy as np
import cv2
import json
import time

begining=time.time()
print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']

    except (KeyError, IndexError) as e:
        input_path = None

images_path=os.path.join(input_path,input_name)


# load images
images = np.load(images_path, allow_pickle=True)


#output path
output_path=os.path.join("..","outputs", "0", "0","output.avi")

writer=None

for frame in images:
    if writer is None:
        # Initialize the video writer
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        writer = cv2.VideoWriter(output_path, fourcc, 30,
                                (frame.shape[1], frame.shape[0]), True)

    writer.write(frame)



print ("[INFO] Cleaning up...")
writer.release()
ending=time.time()

print("running time:",ending-begining)






