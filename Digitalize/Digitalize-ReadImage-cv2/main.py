import cv2
import os
import numpy as np
import json
import time

begining=time.time()
print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        folder_path = os.path.join(input_path, input_name)
    except (KeyError, IndexError) as e:
        input_path = None

input_folder = folder_path

images = os.listdir(input_folder)
images_np = []

start_time=time.time()

for image in images:
    im = cv2.imread(os.path.join(input_folder, image))
    images_np.append(im)

end_tme=time.time()

print("IO NOT INCLUDED running time:",end_tme-start_time)

#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="np_images.npy"
output_file=os.path.join(output_path,output_file)

#Save to file
np.save(output_file,images_np)

endding=time.time()
print("IO INCLUDED running time:",endding-begining)


