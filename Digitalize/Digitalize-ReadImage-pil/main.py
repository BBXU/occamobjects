import os
import numpy as np
import json
from PIL import Image

print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        folder_path = os.path.join(input_path, input_name)
    except (KeyError, IndexError) as e:
        input_path = None

input_folder = folder_path

images = os.listdir(input_folder)
images_np = []

for image in images:
    im = np.array(Image.open(os.path.join(input_folder, image)))
    images_np.append(im)


#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="np_images.npy"
output_file=os.path.join(output_path,output_file)

#Save to file
np.save(output_file,images_np)


