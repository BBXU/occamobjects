import cv2
import os
import numpy as np
import json

print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        folder_path = os.path.join(input_path, input_name)
    except (KeyError, IndexError) as e:
        input_path = None

input_folder = folder_path

# images = os.listdir(input_folder)
video_name=os.listdir(input_folder)[0]
video_path=os.path.join(input_folder,video_name)
images_np = []
vid=cv2.VideoCapture(video_path)

count=0;
while True:
    grabbed, frame = vid.read()
    if not grabbed:
        break
    images_np.append(frame)
    print(count)
    count=count+1
vid.release()

#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="np_images.npy"
output_file=os.path.join(output_path,output_file)

#Save to file
np.save(output_file,images_np)
print("video read in")
