import os
import json
from math import sqrt
import pickle

def euclidean_distance(s1,s2):
    distance=0.0
    for i in range(len(s1)-1):
        distance+=(s1[i]-s2[i])**2

    return sqrt(distance)


def get_neighbors(train_samples, test_sample, num_neighbor):
    distances=list()
    for train_sample in train_samples:
        dist=euclidean_distance(train_sample,test_sample)
        distances.append((train_sample,dist))
    distances.sort(key=lambda tup:tup[1])
    neighbors=list()
    for i in range(num_neighbor):
        neighbors.append(distances[i][0])
    return neighbors

def predict_classification(train,test_row,num_neighbors):
    neighbors=get_neighbors(train,test_row,num_neighbors)
    output_values = [row[-1] for row in neighbors]
    prediction = max(set(output_values), key=output_values.count)
    return prediction

def knn(train_data,test_data,num_neighbors):
    predictions=[]
    for test_sample in test_data:
        predictions.append(predict_classification(train_data,test_sample,num_neighbors))
    return predictions

print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        folder_path = os.path.join(input_path, input_name)
    except (KeyError, IndexError) as e:
        input_path = None


train_data_file=os.path.join(folder_path,"train_data.pickle")
test_data_file=os.path.join(folder_path,"test_data.pickle")

with open(train_data_file,'rb') as file:
    train_data=pickle.load(file)
with open(test_data_file,'rb') as file:
    test_data=pickle.load(file)

predictions=knn(train_data,test_data,5)

#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="predictions.txt"
output_file=os.path.join(output_path,output_file)

#Save to file

with open(output_file,'w') as f:
    for e in predictions:
        f.write(str(e)+"/n")
        print(str(e))




