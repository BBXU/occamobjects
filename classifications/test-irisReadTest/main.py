import os
import json
import numpy as np
import pickle

print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        folder_path = os.path.join(input_path, input_name)
    except (KeyError, IndexError) as e:
        input_path = None

x_train_file=os.path.join(folder_path,"x_train.npy")
x_test_file=os.path.join(folder_path,"x_test.npy")
y_train_file=os.path.join(folder_path,"y_train.npy")
y_test_file=os.path.join(folder_path,"y_test.npy")

x_train=np.load(x_train_file,allow_pickle=True)
x_test=np.load(x_test_file,allow_pickle=True)
y_train=np.load(y_train_file,allow_pickle=True)
y_test=np.load(y_test_file,allow_pickle=True)


pickle_file=os.path.join(folder_path,"train_data.pickle")
with open(pickle_file,"rb") as file:
    a=pickle.load(file)

print(a)

#print(x_train)

