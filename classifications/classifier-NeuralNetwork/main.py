import numpy as np
import pickle
import os
import json

def tanh(x):
    return np.tanh(x)

def tanh_prime(x):
    return 1-np.tanh(x)**2

def mse(y_true, y_pred):
    return np.mean(np.power(y_true-y_pred, 2))

def mse_prime(y_true, y_pred):
    return 2*(y_pred-y_true)/y_true.size

class Layer:
    def __init__(self):
        self.input = None
        self.output = None

    # computes the output Y of a layer for a given input X
    def forward_propagation(self, input):
        raise NotImplementedError

    # computes dE/dX for a given dE/dY (and update parameters if any)
    def backward_propagation(self, output_error, learning_rate):
        raise NotImplementedError

class FCLayer(Layer):
    # input_size = number of input neurons
    # output_size = number of output neurons
    def __init__(self, input_size, output_size):
        self.weights = np.random.rand(input_size, output_size) - 0.5
        self.bias = np.random.rand(1, output_size) - 0.5

    # returns output for a given input
    def forward_propagation(self, input_data):
        self.input = input_data
        self.output = np.dot(self.input, self.weights) + self.bias
        return self.output

    # computes dE/dW, dE/dB for a given output_error=dE/dY. Returns input_error=dE/dX.
    def backward_propagation(self, output_error, learning_rate):
        input_error = np.dot(output_error, self.weights.T)
        weights_error = np.dot(self.input.T, output_error)
        # dBias = output_error

        # update parameters
        self.weights -= learning_rate * weights_error
        self.bias -= learning_rate * output_error
        return input_error

class ActivationLayer(Layer):
    def __init__(self, activation, activation_prime):
        self.activation = activation
        self.activation_prime = activation_prime

    # returns the activated input
    def forward_propagation(self, input_data):
        self.input = input_data
        self.output = self.activation(self.input)
        return self.output

    # Returns input_error=dE/dX for a given output_error=dE/dY.
    # learning_rate is not used because there is no "learnable" parameters.
    def backward_propagation(self, output_error, learning_rate):
        return self.activation_prime(self.input) * output_error



class Network:
    def __init__(self):
        self.layers = []
        self.loss = None
        self.loss_prime = None

    # add layer to network
    def add(self, layer):
        self.layers.append(layer)

    # set loss to use
    def use(self, loss, loss_prime):
        self.loss = loss
        self.loss_prime = loss_prime

    # predict output for given input
    def predict(self, input_data):
        # sample dimension first
        samples = len(input_data)
        result = []

        # run network over all samples
        for i in range(samples):
            # forward propagation
            output = input_data[i]
            for layer in self.layers:
                output = layer.forward_propagation(output)
            result.append(output)

        return result

    # train the network
    def fit(self, x_train, y_train, epochs, learning_rate):
        # sample dimension first
        samples = len(x_train)

        # training loop
        for i in range(epochs):
            err = 0
            for j in range(samples):
                # forward propagation
                output = x_train[j]
                for layer in self.layers:
                    output = layer.forward_propagation(output)

                # compute loss (for display purpose only)
                err += self.loss(y_train[j], output)

                # backward propagation
                error = self.loss_prime(y_train[j], output)
                for layer in reversed(self.layers):
                    error = layer.backward_propagation(error, learning_rate)

            # calculate average error on all samples
            err /= samples
            print('epoch %d/%d   error=%f' % (i+1, epochs, err))


def one_hot(data):
    temp_set=set(data)
    num_cat=len(temp_set)
    one_hot_y=[]
    for i in data:
        temp=[0 for i in range(num_cat)]
        temp[i]=1
        one_hot_y.append(temp)

    return one_hot_y

def modify_input(data):
    x=[]
    y=[]
    for row in data:
        temp_x=row[0:-1]
        temp_y=row[-1]
        x.append(temp_x)
        y.append(temp_y)


    y=one_hot(y)
    return x,y


print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        folder_path = os.path.join(input_path, input_name)
    except (KeyError, IndexError) as e:
        input_path = None


train_data_file=os.path.join(folder_path,"train_data.pickle")
test_data_file=os.path.join(folder_path,"test_data.pickle")

with open(train_data_file,'rb') as file:
    train_data=pickle.load(file)
with open(test_data_file,'rb') as file:
    test_data=pickle.load(file)

train_x,train_y=modify_input(train_data)
test_x,test_y=modify_input(test_data)
# print(train_x)
# print(train_y)

x_train=[]
y_train=[]
for i in range(len(train_x)):
    x_train.append([train_x[i]])
    y_train.append([train_y[i]])


x_train = np.array(x_train)
y_train = np.array(y_train)

x_test=[]
for i in range(len(test_x)):
    x_test.append([test_x[i]])

x_test=np.array(x_test)

# network
net = Network()
net.add(FCLayer(4, 7))
net.add(ActivationLayer(tanh, tanh_prime))
net.add(FCLayer(7, 3))
net.add(ActivationLayer(tanh, tanh_prime))


# train
net.use(mse, mse_prime)
net.fit(x_train, y_train, epochs=1000, learning_rate=0.1)

# test
out = net.predict(x_test)

predictions=[e[0].tolist().index(max(e[0].tolist())) for e in out]
print(predictions)
#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="nn_predictions.txt"
output_file=os.path.join(output_path,output_file)

#Save to file

with open(output_file,'w') as f:
    for e in predictions:
        f.write(str(e)+"\n")

