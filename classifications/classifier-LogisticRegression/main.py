import numpy
import pickle
import os
import json

def sigmoid(x):
    return 1./(1+numpy.exp(x))

def softmax(x):
    e = numpy.exp(x - numpy.max(x))  # prevent overflow
    if e.ndim == 1:
        return e / numpy.sum(e, axis=0)
    else:
        return e / numpy.array([numpy.sum(e, axis=1)]).T  # ndim = 2


class LogisticRegression(object):
    def __init__(self, input, label, n_in, n_out):
        self.x = input
        self.y = label
        self.W = numpy.zeros((n_in, n_out))  # initialize W 0
        self.b = numpy.zeros(n_out)  # initialize bias 0

        # self.params = [self.W, self.b]

    def train(self, lr=0.1, input=None, L2_reg=0.00):
        if input is not None:
            self.x = input

        # p_y_given_x = sigmoid(numpy.dot(self.x, self.W) + self.b)
        p_y_given_x = softmax(numpy.dot(self.x, self.W) + self.b)
        d_y = self.y - p_y_given_x

        self.W += lr * numpy.dot(self.x.T, d_y) - lr * L2_reg * self.W
        self.b += lr * numpy.mean(d_y, axis=0)

        # cost = self.negative_log_likelihood()
        # return cost

    def negative_log_likelihood(self):
        # sigmoid_activation = sigmoid(numpy.dot(self.x, self.W) + self.b)
        sigmoid_activation = softmax(numpy.dot(self.x, self.W) + self.b)

        cross_entropy = - numpy.mean(
            numpy.sum(self.y * numpy.log(sigmoid_activation) +
                      (1 - self.y) * numpy.log(1 - sigmoid_activation),
                      axis=1))

        return cross_entropy

    def predict(self, x):
        # return sigmoid(numpy.dot(x, self.W) + self.b)
        return softmax(numpy.dot(x, self.W) + self.b)

def one_hot(data):
    temp_set=set(data)
    num_cat=len(temp_set)
    one_hot_y=[]
    for i in data:
        temp=[0 for i in range(num_cat)]
        temp[i]=1
        one_hot_y.append(temp)

    return one_hot_y

def modify_input(data):
    x=[]
    y=[]
    for row in data:
        temp_x=row[0:-1]
        temp_y=row[-1]
        x.append(temp_x)
        y.append(temp_y)


    y=one_hot(y)
    return x,y


def train(train_x, train_y, learning_rate=0.01, n_epochs=200):
    x = numpy.array(train_x)
    y = numpy.array(train_y)
    classifier = LogisticRegression(input=x, label=y, n_in=len(train_x[0]), n_out=len(train_y[0]))
    for epoch in range(n_epochs):
        classifier.train(lr=learning_rate)
        learning_rate = learning_rate * 0.95
        print('Training epoch %d' % epoch)

    return classifier



print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']
        folder_path = os.path.join(input_path, input_name)
    except (KeyError, IndexError) as e:
        input_path = None


train_data_file=os.path.join(folder_path,"train_data.pickle")
test_data_file=os.path.join(folder_path,"test_data.pickle")

with open(train_data_file,'rb') as file:
    train_data=pickle.load(file)
with open(test_data_file,'rb') as file:
    test_data=pickle.load(file)

train_x,train_y=modify_input(train_data)
test_x,test_y=modify_input(test_data)

classifier=train(train_x,train_y,0.02,100)


predictions=classifier.predict(test_x)

predictions=[e.tolist().index(max(e.tolist())) for e in predictions]


print(predictions)


#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="lg_predictions.txt"
output_file=os.path.join(output_path,output_file)

#Save to file

with open(output_file,'w') as f:
    for e in predictions:
        f.write(str(e)+"\n")

