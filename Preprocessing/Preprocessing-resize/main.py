import json
import os
import numpy as np
import time
import cv2

begining=time.time()
print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:
        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        input_name = input_information['file']

        option_path=os.path.join(manifest['inputs'][1]['connections'][0]['paths']['mount'], manifest['inputs'][1]['connections'][0]['file'])
    except (KeyError, IndexError) as e:
        input_path = None

with open(option_path) as f:
    option_data=json.load(f)


height=None
width=None
for k,v in option_data.items():
    if k=="height":
        height=v/100
    elif k=="width":
        width=v/100

images_path = os.path.join(input_path, input_name)
print("images_path:",images_path)

# Function code starts here
if images_path == None:
    print("images_path not found")

# initialize result
processed_images = []

# load images
images = np.load(images_path, allow_pickle=True)


start_time=time.time()
for image in images:
    h,w=image.shape[0:2]
    resized_image=cv2.resize(image,(int(w*width),int(h*height)))
    processed_images.append(resized_image)


end_time=time.time()

print("IO NOT INCLUDED running time",end_time-start_time)
#output path
output_path=os.path.join("..","outputs", "0", "0")
output_file="np_images.npy"
output_file=os.path.join(output_path,output_file)

#Save to file
np.save(output_file,processed_images)
ending=time.time()
print("IO INCLUEDE running time",ending-begining)

