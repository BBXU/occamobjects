import os
import json
import sys
from collections import OrderedDict

print("[INFO] --- Collecting paths ---")
with open("../task.json") as f:
    manifest = json.load(f)
    try:

        input_information = manifest['inputs'][0]['connections'][0]
        input_path = input_information['paths']['mount']
        print("input_path:",input_path)
        input_name = input_information['file']
        print("input_name:",input_name)
        folder_path = os.path.join(input_path, input_name)
    except (KeyError, IndexError) as e:
        input_path = None

sys.path.append(folder_path)

from testModule import testObje
a=testObje()
a.print()

